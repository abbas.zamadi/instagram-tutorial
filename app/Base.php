<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Base
{
    protected $table = null;
    public function save($data)
    {
        return DB::table($this->table)->insertGetId($data);
    }
    public function get($data=array())
    {
        $filters = isset($data['filters'])? $data['filters'] : array();
        if ( ! isset($filters['deletedAt']) ) $filters['deletedAt'] = null;
        $fields  = isset($data['fields'])? $data['fields'] : '*';
        return DB::table($this->table)->where($filters)->select($fields)->first();
    }
    public function all( $data=array() )
    {
        $filters = isset($data['filters'])? $data['filters'] : array();
        if ( ! isset($filters['deletedAt']) ) $filters['deletedAt'] = null;
        $whereIn = isset($data['whereIn']) ? $data['whereIn'] : null;
        $fields  = isset($data['fields']) ? $data['fields']  : '*';
        $perPage = isset($data['perPage'])? $data['perPage'] : null;
        $orderBy = isset($data['orderBy'])? $data['orderBy'] : 'id desc';
        $query = DB::table($this->table)->select($fields)->where($filters);
        if ( $whereIn ) $query->whereIn($whereIn['field'], $whereIn['value']);
        $query->orderByRaw($orderBy);
        if ($perPage) {
            return $query->paginate($perPage);
        }else{
            return $query->get();
        }
    }
    public function update($data=array())
    {
        $filters = isset($data['filters'])? $data['filters'] : array('id' => 0);
        $data    = isset($data['data'])? $data['data'] : array();
        $result = DB::table($this->table)->where(array_merge($filters, $data))->select(array('id'))->first();
        if ( $result )
        {
            return true;
        }else{
            return DB::table($this->table)->where($filters)->update($data);
        }
    }
    public function count($data=array())
    {
        $filters = isset($data['filters'])? $data['filters'] : array();
        if ( ! isset($filters['deletedAt'] ) ) $filters['deletedAt'] = null;
        return DB::table($this->table)->where($filters)->count();
    }
    public function softDelete($filters=array('id'=>0))
    {
        $data    = array('deletedAt' => time());
        return DB::table($this->table)->where($filters)->update($data);
    }
}
