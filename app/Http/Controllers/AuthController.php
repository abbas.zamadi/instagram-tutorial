<?php

namespace App\Http\Controllers;

use App\Rules\Phone;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->user = new User();
    }

    public function login(Request $request)
    {
        $phone = $request->post('phone');
        $rules = array(
            'phone' => array('required', new Phone() ),
        );
        $validator  = Validator::make( $request->all(), $rules );
        if ( ! $validator->fails() )
        {
            $phone      = $this->sanitizePhone($phone);
            $filters    = array('phone' => $phone);
            $fields     = array('id', 'phone', 'token');
            $user       = $this->user->get(array('filters' => $filters, 'fields' => $fields));
            if ($user)
            {
                if ( ! $user->token )
                {
                    $verifyCode = rand(1121,9999);
                    $token      = password_hash($verifyCode, PASSWORD_BCRYPT, array('round' => 10));
                    $token      = base64_encode($token);
                    $this->user->update(array('filters' => array('id' => $user->id), 'data' => array('token' => $token)));
                }
                $response = array(
                    'result'    => true,
                    'message'   => 'کد تایید را وارد کنید'
                );
            }else{
                $registerData   = array('phone' => $phone);
                $result         = $this->register($registerData);
                if ($result['result'])
                {
                    $response = array(
                        'result'    => true,
                        'message'   => $result['message'],
                    );
                }else{
                    $response = array(
                        'result'  => false,
                        'message' => $result['message']
                    );
                }
            }
        }else{
            $response = array(
                'result'    => false,
                'errors'    => $validator->errors(),
                'message'   => 'خطاهای زیر را اصلاح کنید'
            );
        }
        return response()->json($response);
    }


    public function verify(Request $request)
    {
        $phone          = $request->get('phone');
        $verifyCode     = $request->get('verifyCode');
        $rules          = array(
            'phone'         => array('required', new Phone()),
            'verifyCode'    => 'required'
        );
        $attributes = array('verifyCode' => 'کد تایید');
        $validator  = Validator::make( $request->all(), $rules, $attributes );
        if ( ! $validator->fails() )
        {
            $phone = $this->sanitizePhone($phone);
            $fields = array('id', 'phone', 'firstName', 'lastName', 'token', 'userStatus');
            $user = $this->user->get( array('filters' => array('phone' => $phone), 'fields' => $fields ));
            if($user){
                if ( $verifyCode == 1111 )
                {
                    if ( $user->userStatus == 0 )
                    {
                        $filters = array('id' => $user->id);
                        $data    = array('userStatus' => 1);
                        $this->user->update(array('filters' => $filters, 'data' => $data));
                    }
                    $response   = array(
                        'result'    => true,
                        'message'   => 'ورود موفقیت آمیز',
                        'data'      => array('user' => $user)
                    );
                }else{
                    $response   = array(
                        'result'    => false,
                        'message'   => 'کد تایید وارد شده نامعتبر است'
                    );
                }
            }else{
                $response = array(
                    'result'    => false,
                    'message'   => 'اطلاعات ارسالی نامعتبر است'
                );
            }
        }else{
            $response = array(
                'result'    => false,
                'errors'    => $validator->errors(),
                'message'   => 'خطاهای زیر را اصلاح کنید'
            );
        }

        return response()->json($response);
    }

    private function register($data)
    {
        $token      = hash('sha256', Str::random(60));
        $phone      = $data['phone'];
        $userData   = array(
            'phone' => $phone,
            'createdAt' => time(),
            'token' => $token,
            'updatedAt' => time()
        );
        if ($userId = $this->user->save($userData)){
            $message = 'ثبت نام شما با موفقیت انجام شد کد تایید را وارد کنید';
            $result  = array(
                'result'    => true,
                'message'   => $message,
            );
            return $result;
        }else{
            return array('result' => false, 'message' => 'خطا در ثبت نام! دوباره تلاش کنید');
        }
    }

    private function sanitizePhone($str)
    {
        $persianNum = array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹');
        for($i=0;$i<10;$i++)
        {
            $str = str_replace($persianNum[$i], $i, $str);
        }

        if ( substr($str, 0, 1) != '0' ){
            $str = '0' . $str;
        }
        return $str;
    }

}
