<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $token   = null;
    public $userId  = null;

    public function authenticate($request)
    {
        //return response()->json('fuck');
        $user       = new User();
        $token      = $request->header('Authorization');
        $filters    = array('token' => $token);
        $fields     = array('id', 'token');
        $user       = $user->get(array('filters' => $filters, 'fields' => $fields));
        if ( $user )
        {
            $this->token    = $user->token;
            $this->userId   = $user->id;
        }else{
            $response = array('result' => false, 'message' => 'اطلاعات ارسال شده نامعتبر است');
            return response()->json($response);
        }
    }

}
