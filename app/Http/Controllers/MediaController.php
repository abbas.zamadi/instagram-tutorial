<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    public function get($mediaName)
    {
        $media      = new Media();
        $mediaName  = trim($mediaName);
        if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $mediaName))
        {
            $filters = array('mediaName' => $mediaName);
            $fields  = array('mediaPath');
            $media   = $media->get(array('filters' => $filters, 'fields' => $fields));
            if ($media)
            {
                return Storage::download($media->mediaPath, $mediaName);
            }else{
                abort(404);
            }
        }else{
            abort(404);
        }
    }
}
