<?php

namespace App\Http\Controllers;

use App\Media;
use App\Tutorial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class TutorialsController extends Controller
{



    public function all(Request $request)
    {
        $tutorial   = new Tutorial();
        $media      = new Media();
        $userId     = $request->get('userId');
        $filters    = array('userId' => $userId);
        $tutorials  = $tutorial->all(array('filters' => $filters));
        $mediaFields= array('id', 'mediaName');
        foreach ( $tutorials as $tutorial )
        {
            $filters            = array('tutorialId' => $tutorial->id, 'type' => 'image');
            $thumb              = $media->get(array('filters' => $filters, 'fields' => $mediaFields));
            $filters['type']    = 'video';
            $video              = $media->get(array('filters' => $filters, 'fields' => $mediaFields));
            $tutorial->thumbnail= $thumb ? route('mediaRoute', $thumb->mediaName) : null;
            $tutorial->video    = $video ? route('mediaRoute', $video->mediaName) : null;
        }
        $response   = array(
            'result'    => true,
            'data'      => array('tutorials' => $tutorials)
        );
        return response()->json($response);
    }

    public function create(Request $request)
    {
        $tutorial   = new Tutorial();
        $media      = new Media();
        $title      = $request->post('title');
        $desc       = $request->post('description');
        $categoryId = $request->post('categoryId');
        $rules      = array(
            'title'         => 'required|string|max:255|min:10',
            'description'   => 'required|string|max:2048|min:20',
            'categoryId'    => 'required|numeric',
            'thumbnail'     => 'required|image|max:512',
            'video'         => 'required|mimes:mp4|max:20000'
        );
        $validator  = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $tutorialData       = array(
                'categoryId'    => $categoryId,
                'userId'        => $request->get('userId'),
                'title'         => $title,
                'description'   => $desc,
                'createdAt'     => time(),
                'updatedAt'     => time()
            );
            if ( $tutorialId = $tutorial->save($tutorialData) )
            {
                try {
                    $thumbUpload    = $request->file('thumbnail')->store('thumbs');
                    $videoUpload    = $request->file('video')->store('videos');
                    $mediaData      = array(
                        'type'      => 'image',
                        'tutorialId'=> $tutorialId,
                        'mediaName' => basename($thumbUpload),
                        'mediaPath' => $thumbUpload,
                        'createdAt' => time()
                    );
                    $media->save($mediaData);
                    $mediaData      = array(
                        'type'      => 'video',
                        'tutorialId'=> $tutorialId,
                        'mediaName' => basename($videoUpload),
                        'mediaPath' => $videoUpload ,
                        'createdAt' => time()
                    );
                    $media->save($mediaData);
                    $response       = array(
                        'result'    => true,
                        'message'   => 'پست با موفقیت تبت شد'
                    );
                }catch (\Exception $e)
                {
                    $tutorial->softDelete(array('id' => $tutorialId));
                    $response       = array(
                        'result'    => false,
                        'message'   => 'خطا در ثبت پست! مجددا تلاش کنید'
                    );
                }
            }else{
                $response = array(
                    'result'    => false,
                    'message'   => 'خطا در ثبت پست! مجددا تلاش کنید'
                );
            }
        } else {
            $response = array(
                'result'    => false,
                'message'   => 'خطا در ذخیره! مجددا تلاش کنید',
                'errors'    => $validator->errors()
            );
        }

        return response()->json($response);
    }

    public function update(Request $request)
    {
        $tutorial   = new Tutorial();
        $title      = $request->post('title');
        $desc       = $request->post('description');
        $categoryId = $request->post('categoryId');
        $tutorialId = $request->post('tutorialId');
        $rules      = array(
            'title'         => 'required|string|max:255|min:10',
            'description'   => 'required|string|max:2048|min:20',
            'categoryId'    => 'required|numeric',
            'tutorialId'    => 'required|numeric',
        );
        $validator  = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $tutorialData       = array(
                'categoryId'    => $categoryId,
                'userId'        => $request->get('userId'),
                'title'         => $title,
                'description'   => $desc,
                'updatedAt'     => time()
            );
            $filters    = array('id' => $tutorialId);
            $tutorialO  = $tutorial->get(array('filters' => $filters));
            if ( $tutorialO and $tutorialO->userId == $request->get('userId') )
            {
                $filters = array('id' => $tutorialO->id);
               if ( $tutorial->update(array('filters' => $filters, 'data' => $tutorialData)))
               {
                   $response = array(
                       'result'     => true,
                       'message'    => 'پست با موفقیت ویرایش شد'
                   );
               }else{
                   $response = array(
                       'result'     => false,
                       'message'    => 'خطا در ثبت تغییرات! دوباره تلاش کنید'
                   );
               }
            }else{
                $response = array(
                    'result'    => false,
                    'message'   => 'اطلاعات نامعتبر'
                );
            }
        } else {
            $response = array(
                'result'    => false,
                'message'   => 'خطاهای زیر را اصلاح کنید',
                'errors'    => $validator->errors()
            );
        }

        return response()->json($response);
    }

    public function delete(Request $request)
    {
        $tutorial   = new Tutorial();
        $tutorialId = $request->post('tutorialId');
        $rules      = array(
            'tutorialId'    => 'required|numeric',
        );
        $validator  = Validator::make($request->all(), $rules);
        if ( ! $validator->fails() )
        {
            $filters    = array('id' => $tutorialId, 'userId' => $request->get('userId'));
            $tutorialO  = $tutorial->get(array('filters' => $filters));
            if ( $tutorialO )
            {
                if ( $tutorial->softDelete($filters))
                {
                    $response = array(
                        'result'    => true,
                        'message'   => 'پست با موفقیت حذف شد'
                    );
                }else{
                    $response = array(
                        'result'    => false,
                        'message'   => 'خطا در حذف پست! مجددا تلاش کنید'
                    );
                }
            }else{
                $response = array(
                    'result'    => false,
                    'message'   => 'اطلاعات ارسالی نامعتبر است'
                );
            }
        }else{
            $response = array(
                'result'    => false,
                'message'   => 'خطاهای زیر را اصلاح کنید',
                'errors'    => $validator->errors()
            );
        }
        return response()->json($response);

    }


}
