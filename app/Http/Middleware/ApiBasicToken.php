<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ApiBasicToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user  = new User();
        $token = $request->bearerToken();
        if ( $token )
        {
            $filters = array('token' => $token);
            $fields  = array('id', 'token');
            $user    = $user->get(array('filters' => $filters, 'fields' => $fields));
            if ( $user )
            {
                $request->merge(array('userId' => $user->id, 'token' => $token));
                return $next($request);
            }else{
                $response = array(
                    'result'    => false,
                    'message'   => 'درخواست نامعتبر'
                );
                return response()->json($response);
            }
        }else{
            $response = array(
                'result'    => false,
                'message'   => 'درخواست نامعتبر'
            );
            return response()->json($response);
        }
    }
}
