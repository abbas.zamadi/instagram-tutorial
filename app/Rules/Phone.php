<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Phone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $phoneReg = '/^(\+98|0)?9\d{9}$/i';
        $str = $this->num2en($value);
        if (preg_match($phoneReg, $str)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'شماره موبایل معتبر نیست.';
    }


    /**
     * convert persian digits to english digits.
     *
     * @param $string
     * @return string
     */
    private function num2en($string)
    {
        $persianNum = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        for ($i = 0; $i < 10; $i++) {
            $string = str_replace($persianNum[$i], $i, $string);
        }
        return $string;
    }
}
