<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->id();
            $table->foreignId('userId')->unsigned();
            $table->foreignId('categoryId')->unsigned();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('numLikes')->default(0);
            $table->unsignedInteger('numDislikes')->default(0);
            $table->unsignedInteger('createdAt');
            $table->unsignedInteger('updatedAt');
            $table->unsignedInteger('publishedAt')->nullable();
            $table->unsignedInteger('deletedAt')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorials');
    }
}
