<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function(){
    Route::post('login', 'AuthController@login');
    Route::post('verify', 'AuthController@verify');
});
Route::group(['prefix' => 'tutorials', 'middleware' => 'baseToken'], function() {
    Route::get('all', 'TutorialsController@all');
    Route::post('create', 'TutorialsController@create');
    Route::post('update', 'TutorialsController@update');
    Route::post('delete', 'TutorialsController@delete');
});

Route::group(['prefix' => 'media'], function() {
    Route::get('{name}', 'MediaController@get')->name('mediaRoute');
});




